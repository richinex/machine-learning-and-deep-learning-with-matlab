function displaySequence(X,Y)
clrOrder = get(gca,'ColorOrder');
classNames = categories(removecats(Y));
grps = findgroups(Y);
gscatter(1:length(X),X,grps,clrOrder(1:3,:))
legend(classNames)
xlabel('Time (samples)')
ylabel('')
end
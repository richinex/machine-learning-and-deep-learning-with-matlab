function m = dummifyText(t)
    vocab = uint8(' !"&''()*,-.0123456789:;?_abcdefghijklmnopqrstuvwxyz');
    m = dummyvar(categorical(uint8(t),vocab)')';
end
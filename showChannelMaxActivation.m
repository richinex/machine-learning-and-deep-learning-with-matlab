% This function displays the original image and the activations for one channel.
function showChannelMaxActivation(origImage,act)
imageSize = size(origImage);
imageSize = imageSize([1 2]);

[~,maxValueIndex] = max(max(max(act)));

act_chMax = act(:,:,maxValueIndex);
act_chMax = imresize(mat2gray(act_chMax), imageSize);
imshowpair(origImage,act_chMax,'montage')
end
% This function modifies an input image using a vector of 3 RGB values.
function fixedim = correctColor(im,rgb)
    fixedim = uint8(single(im) - reshape(rgb,[1 1 3]));
end

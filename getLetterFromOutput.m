function t = getLetterFromOutput(m)
    vocab = [newline char(13) ' !"&''(),-.01234678:;?ABCDEFGHIJKLMNOPQRSTUVWY[]_`abcdefghijklmnopqrstuvwxyz'];
    [~,e] = max(m);
    t = vocab(e);
end

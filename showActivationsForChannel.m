% This function displays the original image and the activations for one channel.
function showActivationsForChannel(origImage,ac,ch)
imageSize = size(origImage);
imageSize = imageSize([1 2]);

act_ch = ac(:,:,ch);
act_ch = imresize(mat2gray(act_ch),imageSize);
imshowpair(origImage,act_ch,'montage')
title(['Compare Channel ',num2str(ch)])
end